class AddApplicantsTable < ActiveRecord::Migration[5.1]
  def change
    create_table :applicants do |t|
      t.string :name
      t.string :surname
      t.string :city
      t.string :address
      t.integer :telephone
      t.string :email
      t.integer :total_members
      t.integer :incomings
      t.text :description
      t.timestamps
    end
  end
end
