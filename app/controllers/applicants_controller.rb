class ApplicantsController < ApplicationController

  def new
    @applicant = Applicant.new
  end
  
  def index
    @applicants = Applicant.all
  end

  def create
    @applicant = Applicant.new(applicant_params)
    if @applicant.save
      redirect_to root_path
    else
      redirect_to root_path
    end

  end


  def applicant_params
    params.require(:applicant).permit!
  end

end
